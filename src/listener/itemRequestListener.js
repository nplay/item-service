
class ItemRequestListener
{
    async execute(content, message, ok, fail){

        try {
            var identifier = content.params?.identifier
            var itemServiceBody = content.params?.itemServiceBody

            if(!identifier)
                throw new Error(`Identifier not found`)
            
            if(!itemServiceBody)
                throw new Error(`itemServiceBody not found`)

            this.broker.call('v1.item.publish', {
                identifier: identifier,
                params: itemServiceBody.params
            })

            this.emit2({}, 'v1.item.product.itemRequestAfter', {
                identifier: identifier
            })

            ok(message)

        } catch (error) {
            
            this.emit2({}, 'v1.any.fatalError', {
                identifier: identifier,
                context: 'Falha ao publicar itens no banco de dados',
                subject: `${identifier} integração de item falhou`,
                error: error.message,
                attachments: [
                    {
                        filename: 'data.log',
                        content: JSON.stringify({identifier: identifier, itemServiceBody: itemServiceBody}),
                        contentType: 'text/plain'
                    },
                    {
                        filename: 'stackTrace.log',
                        content: error.stack,
                        contentType: 'text/plain'
                    }
                ]
            })

            fail(message)
        }

    }
}

export {
    ItemRequestListener
}