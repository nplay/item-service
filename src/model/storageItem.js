var mongoose = require("mongoose")

var schema = new mongoose.Schema(
    {
        created_at: Date,
        entityId: String
    },
    {
        strict: false
    }
);

let nameModel = "StorageItem";

class ModelClass {
}

schema.loadClass(ModelClass);

export default (connection) => {
    return connection.model(nameModel, schema)
}