const { ServiceBroker } = require("moleculer");
const ApiService = require("moleculer-web");
const E = require("moleculer-web").Errors;

import {Config} from "core"

ApiService.settings.port = 3004
global.broker = new ServiceBroker({
    nodeID: process.env.HOSTNAME,
    transporter: Config.queue.driver.rabbitMQ.connectionString
});

import ItemService from './itemService'

let fnRun = async () => {

    // Create a service
    broker.createService(ItemService);

    // Load API Gateway
    broker.createService({
        mixins: [ApiService],
        settings: {
            cors: {
                // Configures the Access-Control-Allow-Origin CORS header.
                origin: "*",
                // Configures the Access-Control-Allow-Methods CORS header. 
                methods: ["GET", "OPTIONS", "POST", "PUT", "DELETE"],
                // Configures the Access-Control-Allow-Headers CORS header.
                //allowedHeaders: [],
            },

            routes: [
                {
                    path: "/",
                    authorization: true,
    
                    onBeforeCall(ctx, route, req, res) {
                        // Set request headers to context meta
                        ctx.meta.headers = req.headers;
                    },

                    bodyParsers: {
                        json: { limit: "1MB" }
                    },
                }
            ]
        },
        methods: {
            async authorize(ctx, route, req, res) {
                
                // Read the token from header
                let token = req.headers["authorization-token"];

                if(!token)
                    return Promise.reject(new E.UnAuthorizedError(E.ERR_NO_TOKEN));

                let response = await broker.call('v1.auth.validate', {token: token});

                if(response.body.success == false || response.body.allowed == false)
                    return Promise.reject(new E.UnAuthorizedError(E.ERR_INVALID_TOKEN));
                
                ctx.meta.tokenPayload = JSON.parse(response.body.tokenPayload)
                return Promise.resolve(ctx);
            }
        }
    });

    // Start server
    await broker.start();
}

fnRun()