
const AMQPMixin = require("moleculer-amqp-queue");
const mongoose = require('mongoose');

AMQPMixin.settings.url = Config.queue.driver.rabbitMQ.connectionString

const QueueService = require("moleculer-amqp-queue");
import {Config} from "core"
import events2 from 'event2'

import StorageItem from './model/storageItem'
import { ItemRequestListener } from "./listener/itemRequestListener";

export default {
    name: "item",
    version: 1,

    mixins: [QueueService, events2],

    AMQPQueues: {
        "ecommerce_item_publish"(channel, msg) {
            let fnProcess = async () => {
                try {
                    let job = JSON.parse(msg.content.toString());
                    var conn = await this.conn(job.identifier)

                    for(let param of job.params || [])
                    {
                        try {
                            let methodInvoke = `${param.action}Item`
                            await this[methodInvoke](conn, param.item)
                        } catch (error) {
                            console.error(error.toString())
                        }
                    }
    
                    channel.ack(msg);
                } finally {
                    conn.close()
                }
            }

            fnProcess()
        }
    },

    methods: {

        async conn(database) {
            let db = Config.database
            let connString = `mongodb://${db.host}:${db.port}/${database}`

            return mongoose.createConnection(connString, {
                poolSize: 1
            })
        },

        async updateItem(conn, item)
        {
            await StorageItem(conn).updateOne({
                entityId: item.entityId + ''
            }, { $set: item }, {upsert: true})
        },

        async deleteItem(conn, item)
        {
            let collection = conn.db.collection('storageitems')

            if(!item.entityId)
                return false

            let id = item.entityId + ''

            await collection.remove({ entityId: id })
        },

        async insertItem(conn, item)
        {
            await StorageItem(conn).updateOne({
                entityId: item.entityId + ''
            }, { $set: item }, {upsert: true})
        }
    },

    actions: {

        async publish(ctx) {
            try {
                let params = ctx.params.params

                this.addAMQPJob("ecommerce_item_publish", {
                    identifier: ctx.params.identifier,
                    params: params
                });

                return true
            } catch (error) {
                console.log(error)
                return false
            }
        }
    },

    events2: [
        {
            event:'v1.ipapi.product.itemRequest',
            listeners: {
                retrive: {
                    handle: new ItemRequestListener().execute
                }
            }
        }
    ]
}